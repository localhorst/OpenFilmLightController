#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QIntValidator>
#include "lightmux.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void setLablePowerCold(int val);
    void setLablePowerWarm(int val);
    void setLablePowerCombined(int val);
    void setLableResistanceCold(int val);
    void setLableResistanceWarm(int val);

private slots:
    void on_BtnIncreaseBrightness_clicked();

    void on_BtnDecreaseBrightness_clicked();

    void on_BtnIncreaseTemperatur_clicked();

    void on_BtnDecreaseTemperatur_clicked();

    void on_BtnSetBrightness_clicked();

    void on_BtnSetTemperatur_clicked();

    void on_lineBrightness_returnPressed();

    void on_lineTemperature_returnPressed();

    void on_checkBox_stateChanged();

private:
    Ui::MainWindow *ui;
    LightMux *lmux;

    void updateUI();
};

#endif // MAINWINDOW_H
