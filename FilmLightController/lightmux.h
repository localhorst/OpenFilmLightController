#ifndef LIGHTMUX_H
#define LIGHTMUX_H
#include <QDebug>

class LightMux
{
private:

    struct range {
        double low;
        double high;
    };

    struct powermix {
        int cold;
        int warm;
    };

    int brightness; // total power of the LED Panels (both colors) in percent
    int temperatur; // muxed color temperature of the LED Panels, from coldest to warmest in Kelvin

    int coldest_temperature = 6500; // coldest possible temperature in Kelvin --> the cold LEDs
    int warmest_temperature = 2700; // warmest possible temperature in Kelvin --> the warm LEDs
    int steps_temperature = 100; // steps to change the temperature in Kelvin

    int power_cold; // power of the cold LEDs of Panels in percent
    int power_warm; // power of the warm LEDs of Panels in percent
    int power_combined; //combined power of both LEDs in percent

    int resistance_cold; // Resistance of the DigiPot on Driver-Board
    int resistance_warm; // Resistance of the DigiPot on Driver-Board

    int fullCommonColor; // color mix with both LED´s at same brightess

    bool adaptive_brightness; // true indecates adaptive brightness is enabled

    struct range cold_resistance_range;
    struct range warm_resistance_range;

public:
    LightMux();

    void incBrightness();
    void decBrightness();

    void setBrightness(int val); // set total power of the LED Panels (both colors) in percent

    void cold_Temperature();
    void warm_Temperature();

    void setTemperature(int requested_val); // set muxed color temperature of the LED Panels, from coldest to warmest in Kelvin. Fitted to the nearest value that the steps allow

    int getBrightness();

    int getTemperature();

    int getColdest_temperature();
    int getWarmest_temperature();

    void mux();

    int getPowerCold();
    int getPowerWarm();

    int getPowerCombined();

    int getResistanceCold();
    int getResistanceWarm();

    void toogleAdaptiveBrightness();

private:
    struct powermix mux_linear(); // brightness will vary by different color temperature
    struct powermix mux_adaptiv(); // same brightness by different color temperature
    double map(double value, double fromLow, double fromHigh, double toLow, double toHigh); // remaps the value
};

#endif // LIGHTMUX_H
