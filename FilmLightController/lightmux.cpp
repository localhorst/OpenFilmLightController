#include "lightmux.h"

LightMux::LightMux()
{
    temperatur = warmest_temperature;
    brightness = 100;
    power_cold = 0;
    power_warm = 0;
    power_combined = 0;
    adaptive_brightness = false;
    fullCommonColor = ((warmest_temperature + coldest_temperature) / 2);
    qDebug("full common color: %i\n", fullCommonColor);

    cold_resistance_range.low = 100.0;
    cold_resistance_range.high = 900.0;

    warm_resistance_range.low = 100.0;
    warm_resistance_range.high = 900.0;

}


void LightMux::mux(){

    struct powermix mixed;

    if(adaptive_brightness == true){
        qDebug("adaptiv\n");
        mixed =  mux_adaptiv();
    } else {
        qDebug("linear\n");
        mixed =  mux_linear();
    }

    power_cold = static_cast<int>(double(mixed.cold * (double(brightness/100.0))) + .5);
    power_warm = static_cast<int>(double(mixed.warm * (double(brightness/100.0))) + .5);

    qDebug("cold color: %i\nwarm color: %i\n", power_cold, power_warm);

    power_combined = static_cast<int>(double(power_cold+power_warm) / 2.0);


    resistance_cold = static_cast<int>(double(map(power_cold, 0.0, 100.0, cold_resistance_range.low, cold_resistance_range.high)));

    resistance_warm = static_cast<int>(double(map(power_warm, 0.0, 100.0, warm_resistance_range.low, warm_resistance_range.high)));



}

struct LightMux::powermix LightMux::mux_linear() {

    struct powermix tmp;

    if(temperatur == fullCommonColor){
        qDebug("same as full common color\n");
        tmp.cold = 100;
        tmp.warm = 100;

    } else if(temperatur < fullCommonColor) {
        qDebug("lower as full common color\n");
        // warm led dominant
        // cold led recessive
        tmp.warm = 100;
        double a = double(((temperatur * (tmp.warm/100)) - ((tmp.warm/100) * warmest_temperature)));
        double b = double((coldest_temperature - temperatur));
        double c = a / b * 100;
        tmp.cold = static_cast<int>(c + .5);
    } else if(temperatur > fullCommonColor) {
        qDebug("higher as full common color\n");
        // cold led dominant
        // warm led recessive
        tmp.cold = 100;
        double a = double(((temperatur * (tmp.cold/100)) - ((tmp.cold/100) * coldest_temperature)));
        double b = double((warmest_temperature - temperatur));
        double c = a / b * 100;
        tmp.warm = static_cast<int>(c + .5);
    }

    return tmp;

}


struct LightMux::powermix LightMux::mux_adaptiv(){

    struct powermix tmp;

    if(temperatur == fullCommonColor){
        qDebug("same as full common color\n");
        power_cold = 50;
        power_warm = 50;
        tmp.cold = 50;
        tmp.warm = 50;

    } else if(temperatur < fullCommonColor) {
        qDebug("lower as full common color\n");
        // warm led dominant
        // cold led recessive
        double a = double((temperatur) - (warmest_temperature));
        double b = double((-warmest_temperature + coldest_temperature));
        double c = a / b * 100;
        tmp.cold = static_cast<int>(c + .5);
        tmp.warm = 100 - tmp.cold;
    } else if(temperatur > fullCommonColor) {
        qDebug("higher as full common color\n");
        // cold led dominant
        // warm led recessive

        double a = double((temperatur) - (coldest_temperature));
        double b = double((-coldest_temperature + warmest_temperature));
        double c = a / b * 100;
        tmp.warm = static_cast<int>(c + .5);
        tmp.cold = 100 - tmp.warm;
    }

    return tmp;

}


void LightMux::decBrightness(){
    brightness--;
    if(brightness < 0){
        brightness = 0;
    }
}

void LightMux::incBrightness(){
    brightness++;
    if(brightness > 100){
        brightness = 100;
    }
}

void LightMux::setBrightness(int val){

    brightness = val;

    if(brightness < 0){
        brightness = 0;
    }

    if(brightness > 100){
        brightness = 100;
    }

}

void LightMux::warm_Temperature(){
    temperatur = temperatur - steps_temperature;
    if(temperatur < warmest_temperature){
        temperatur = warmest_temperature;
    }
}

void LightMux::cold_Temperature(){
    temperatur = temperatur + steps_temperature;
    if(temperatur > coldest_temperature){
        temperatur = coldest_temperature;
    }
}

void LightMux::setTemperature(int requested_val){

    int current_temp = warmest_temperature;

    /* out of range */

    if(requested_val < warmest_temperature){
        temperatur = warmest_temperature;
        return;
    }

    if(requested_val > coldest_temperature){
        temperatur = coldest_temperature;
        return;
    }

    /* between range, map to steps */

    while(true){
        if(requested_val <= current_temp){

            temperatur = current_temp;

            int diff = current_temp - requested_val;

            if(diff > (steps_temperature/2)){
                warm_Temperature();
            }

            return;
        }
        current_temp = current_temp + steps_temperature;
    }
}

void LightMux::toogleAdaptiveBrightness(){
    adaptive_brightness = !adaptive_brightness;

    if(adaptive_brightness){
        qDebug("adaptive brightness enabled\n");
    } else{
        qDebug("adaptive brightness disabled\n");
    }
}

double LightMux::map(double value, double fromLow, double fromHigh, double toLow, double toHigh){
    return toLow + (toHigh - toLow) * ((value - fromLow) / (fromHigh - fromLow));
}

int LightMux::getBrightness(){
    return brightness;
}

int LightMux::getTemperature(){
    return temperatur;
}

int LightMux::getColdest_temperature(){
    return coldest_temperature;
}

int LightMux::getWarmest_temperature(){
    return warmest_temperature;
}

int LightMux::getPowerCold(){
    return power_cold;
}

int LightMux::getPowerWarm(){
    return power_warm;
}

int LightMux::getPowerCombined(){
    return power_combined;
}

int LightMux::getResistanceCold(){
    return resistance_cold;
}

int LightMux::getResistanceWarm(){
    return resistance_warm;
}
