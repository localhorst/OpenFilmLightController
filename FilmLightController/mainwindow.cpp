#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setFixedSize(400,360);

    lmux = new LightMux();

    ui->lcdBrightness->setDigitCount(3);
    ui->lcdTemperature->setDigitCount(4);
    ui->lineBrightness->setValidator(new QIntValidator(0, 100, this));
    ui->lineTemperature->setValidator(new QIntValidator(lmux->getWarmest_temperature(), lmux->getColdest_temperature(), this));

    lmux->mux();

    updateUI();

}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::updateUI()
{
    ui->lcdBrightness->display(lmux->getBrightness());
    ui->lcdTemperature->display(lmux->getTemperature());
    ui->lineTemperature->clear();
    ui->lineBrightness->clear();

    setLablePowerCold(lmux->getPowerCold());
    setLablePowerWarm(lmux->getPowerWarm());
    setLablePowerCombined(lmux->getPowerCombined());
    setLableResistanceCold(lmux->getResistanceCold());
    setLableResistanceWarm(lmux->getResistanceWarm());
}


void MainWindow::setLablePowerCold(int val)
{
    ui->labelPowerCold->setText("Output Power Cold: " + QString::number(val) + "%");
}

void MainWindow::setLablePowerWarm(int val)
{
    ui->labelPowerWarm->setText("Output Power Warm: " + QString::number(val) + "%");
}

void MainWindow::setLablePowerCombined(int val)
{
    ui->labelPowerCombined->setText("Combined Power: " + QString::number(val) + "%");
}

void MainWindow::setLableResistanceCold(int val){
    ui->labelResistanceCold->setText("Resistance Cold: " + QString::number(val) + "Ω");
}

void MainWindow::setLableResistanceWarm(int val){
    ui->labelResistanceWarm->setText("Resistance Warm: " + QString::number(val) + "Ω");
}

void MainWindow::on_BtnIncreaseBrightness_clicked()
{
    lmux->incBrightness();
    lmux->mux();
    updateUI();
}

void MainWindow::on_BtnDecreaseBrightness_clicked()
{
    lmux->decBrightness();
    lmux->mux();
    updateUI();
}

void MainWindow::on_BtnIncreaseTemperatur_clicked()
{
    lmux->cold_Temperature();
    lmux->mux();
    updateUI();
}

void MainWindow::on_BtnDecreaseTemperatur_clicked()
{
    lmux->warm_Temperature();
    lmux->mux();
    updateUI();
}

void MainWindow::on_BtnSetBrightness_clicked()
{
    lmux->setBrightness(ui->lineBrightness->text().toInt());
    lmux->mux();
    updateUI();
}

void MainWindow::on_BtnSetTemperatur_clicked()
{
    lmux->setTemperature(ui->lineTemperature->text().toInt());
    lmux->mux();
    updateUI();
}


void MainWindow::on_lineBrightness_returnPressed()
{
    on_BtnSetBrightness_clicked();
}

void MainWindow::on_lineTemperature_returnPressed()
{
    on_BtnSetTemperatur_clicked();
}



void MainWindow::on_checkBox_stateChanged()
{
    lmux->toogleAdaptiveBrightness();
    lmux->mux();
    updateUI();
}
